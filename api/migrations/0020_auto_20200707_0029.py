# Generated by Django 3.0.7 on 2020-07-06 19:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0019_remove_fileuploadmodel_extension'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fileuploadmodel',
            name='file',
            field=models.FileField(upload_to='file_upload_model'),
        ),
    ]
