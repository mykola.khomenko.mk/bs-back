from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
from social_core.utils import slugify

from backend import settings


def generate_unique_slug(klass, field):
    """
    return unique slug if origin slug is exist.
    eg: `foo-bar` => `foo-bar-1`

    :param `klass` is Class model.
    :param `field` is specific field for title.
    """
    origin_slug = slugify(field)
    unique_slug = origin_slug
    numb = 1
    while klass.objects.filter(slug=unique_slug).exists():
        unique_slug = '%s-%d' % (origin_slug, numb)
        numb += 1
    return unique_slug


class DtModelDefault(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        ordering = ('-created',)


class FileUploadModel(DtModelDefault):
    title = models.CharField(max_length=255, null=True, blank=True)
    file = models.FileField(upload_to="file_upload_model", null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def get_absolute_url(self):
        if self.file:
            return settings.BACKEND_URL + self.file.url
        return None

    def __str__(self):
        if self.user:
            return self.file.name + ' (' + self.user.username + ')'
        return self.file.name


class Tariff(models.Model):
    name = models.CharField(max_length=50, null=True)
    day_count = models.IntegerField(default=30)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=30, blank=True)
    second_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    phone = models.CharField(max_length=20, blank=True)
    position = models.CharField(max_length=100, blank=True)
    serial_and_number_passport = models.CharField(max_length=15, blank=True)
    date_of_issue_passport = models.DateField(null=True, blank=True)
    image = models.ForeignKey(FileUploadModel, null=True, blank=True, on_delete=models.CASCADE)
    type_account = models.SmallIntegerField(default=0, blank=True)
    tariff = models.ForeignKey(Tariff, null=True, blank=True, on_delete=models.SET_NULL)
    tariff_close_dt = models.DateField(null=True, blank=True)
    is_subscriber = models.BooleanField(default=True)

    def __str__(self):
        return self.user.username

    def get_admin_user_info(self):
        return self.user.username \
               + ' (' + self.first_name \
               + ' ' + self.last_name + ')'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class CountModelDefault(models.Model):
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    ip = models.GenericIPAddressField(null=True, blank=True)

    class Meta:
        abstract = True


class Rating(CountModelDefault):
    value = models.PositiveIntegerField(validators=[MinValueValidator(0.0), MaxValueValidator(5.0)], null=True)


class Like(CountModelDefault):
    pass


class Bookmark(CountModelDefault):
    pass


class View(CountModelDefault):
    pass


class StatisticDefault(DtModelDefault):
    likes = models.ManyToManyField(Like, null=True, blank=True)
    views = models.ManyToManyField(View, null=True, blank=True)
    bookmarks = models.ManyToManyField(Bookmark, null=True, blank=True)

    class Meta:
        abstract = True


class Category(models.Model):
    title = models.CharField(max_length=255, null=True)
    parent = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Video(DtModelDefault):
    # TODO: Проверять типы с vue. Там без автоматического получения
    TYPES = [
        (0, 'Youtube'),
        (1, 'Sale'),
        (2, 'CashBack'),
        (3, 'Coupon'),
    ]
    title = models.CharField(max_length=255, null=True)
    description = models.CharField(max_length=500, null=True)
    type = models.SmallIntegerField(default=0, choices=TYPES)
    categories = models.ManyToManyField(Category)
    file = models.ForeignKey(FileUploadModel, null=True, on_delete=models.CASCADE)
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)


class Service(models.Model):
    title = models.CharField(max_length=255, null=True, unique=True)
    categories = models.ManyToManyField(Category)


class Company(DtModelDefault):
    title = models.CharField(max_length=150, null=False)
    ogrn = models.CharField(max_length=30, null=False)
    inn = models.CharField(max_length=30, null=False)
    kpp = models.CharField(max_length=30, null=False)
    bik = models.CharField(max_length=30, null=False)
    bank_account = models.CharField(max_length=30, null=False)
    license = models.CharField(max_length=30, null=False)
    fio_director = models.CharField(max_length=90, null=False)
    city = models.CharField(max_length=255, null=False)
    address = models.CharField(max_length=300, null=False)
    phone = models.CharField(max_length=20, null=False)
    email = models.CharField(max_length=50, null=False)
    site = models.CharField(max_length=30, null=True)
    count_clinics = models.IntegerField(default=0)
    description = models.TextField(null=False)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    logo = models.ForeignKey(FileUploadModel, null=True, blank=True, on_delete=models.CASCADE)


class Clinic(StatisticDefault):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    address = models.CharField(max_length=300, null=True)
    phone = models.CharField(max_length=20, null=True)
    email = models.CharField(max_length=50, null=True)
    site = models.CharField(max_length=30, null=True)
    title = models.CharField(max_length=150, null=True)
    description = models.CharField(max_length=255, null=True)
    content = models.TextField(null=True)
    logo = models.ForeignKey(FileUploadModel, null=True, blank=True, on_delete=models.CASCADE)
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    ratings = models.ManyToManyField(Rating)
    moderators = models.ManyToManyField(Profile)


class ClinicService(StatisticDefault):
    categories = models.ManyToManyField(Category)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    clinic = models.ForeignKey(Clinic, on_delete=models.CASCADE)


class Coupon(StatisticDefault):
    description = models.CharField(max_length=255, null=True)
    coupon = models.CharField(max_length=20, null=True)
    max_count_activation = models.PositiveIntegerField(default=30)
    current_count_activation = models.PositiveIntegerField(default=30)
    finish_dt = models.DateField(null=True)
    clinic = models.ForeignKey(Clinic, null=True, blank=True, on_delete=models.CASCADE)
    service = models.ForeignKey(ClinicService, null=True, blank=True, on_delete=models.CASCADE)


class Sale(StatisticDefault):
    description = models.CharField(max_length=255, null=True)
    finish_dt = models.DateField(null=True)
    clinic = models.ForeignKey(Clinic, null=True, blank=True, on_delete=models.CASCADE)
    service = models.ForeignKey(ClinicService, null=True, blank=True, on_delete=models.CASCADE)


class Page(StatisticDefault):
    page_title = models.CharField(max_length=60, null=True, blank=True)
    page_description = models.CharField(max_length=160, null=True, blank=True)
    page_keywords = models.CharField(max_length=255, null=True, blank=True)
    title = models.CharField(max_length=255, null=True)
    description = models.CharField(max_length=255, null=True)
    content = models.TextField(null=True)
    is_accept = models.BooleanField(default=False)

    class Meta:
        abstract = True


class News(Page):
    pass


class Tag(models.Model):
    title = models.CharField(max_length=50, null=True)


class Blog(Page):
    tags = models.ManyToManyField(Tag, blank=True)
    categories = models.ManyToManyField(Category)
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    is_accept = models.BooleanField(default=True)
    image = models.ForeignKey(FileUploadModel, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Faq(Page):
    TYPES = [
        (0, 'Для всех'),
        (1, 'Для пользователей'),
        (2, 'Для партнеров'),
    ]
    title_mini = models.CharField(max_length=60, null=True)
    description_mini = models.CharField(max_length=100, null=True)
    icon = models.CharField(max_length=30, null=True)
    cls = models.CharField(max_length=7, null=True)
    type = models.SmallIntegerField(default=0, choices=TYPES)
    slug = models.SlugField(max_length=255, unique=True, null=True, blank=True)

    def get_type_str(self):
        return str(self.TYPES[self.type][1])

    def __str__(self):
        return self.title + ' (' + self.get_type_str() + ')'

    def save(self, *args, **kwargs):
        if self.slug is None:
            self.slug = generate_unique_slug(Faq, self.title)
        super(Faq, self).save(*args, **kwargs)


class Feedback(DtModelDefault):
    STATUSES = [
        (0, 'Open'),
        (1, 'Close'),
    ]
    name = models.CharField(max_length=100, null=True)
    phone = models.CharField(max_length=20, null=True)
    status = models.SmallIntegerField(default=0, choices=STATUSES)


class Support(DtModelDefault):
    STATUSES = [
        (0, 'Open'),
        (1, 'Close'),
    ]
    title = models.CharField(max_length=255, null=False)
    description = models.CharField(max_length=500, null=False)
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    status = models.SmallIntegerField(default=0, choices=STATUSES)


class SupportItem(DtModelDefault):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    support = models.ForeignKey(Support, on_delete=models.CASCADE)
    message = models.TextField(null=True)
    files = models.ManyToManyField(FileUploadModel, null=True, blank=True)

    def __str__(self):
        return 'user: ' + (self.user.username if self.user else 'not') \
               + ' | support: ' + str(self.support.id)

    class Meta:
        ordering = ('created',)


class Notification(DtModelDefault):
    TYPES = [
        (0, 'Default'),
        (1, 'Information'),
        (2, 'Warning'),
        (3, 'Danger'),
    ]
    title = models.CharField(max_length=100, null=True)
    description = models.CharField(max_length=255, null=True)
    type = models.SmallIntegerField(default=0, choices=TYPES)
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    is_read = models.BooleanField(default=False)

    def __str__(self):
        return self.title + '(' + self.user.username + ')'


class History(DtModelDefault):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    service = models.ForeignKey(ClinicService, on_delete=models.CASCADE)


class Info(DtModelDefault):
    name = models.CharField(max_length=30, null=True, unique=True)
    value = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.name


class Banner(DtModelDefault):
    title = models.CharField(max_length=255, null=True)
    description = models.CharField(max_length=500, null=True)
    categories = models.ManyToManyField(Category)
    file = models.ForeignKey(FileUploadModel, null=True, on_delete=models.CASCADE)
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
