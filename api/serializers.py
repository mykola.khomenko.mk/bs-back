from django.contrib.auth.models import User, Group
from drf_extra_fields.fields import Base64ImageField, Base64FileField
from rest_framework import serializers

from api.fields import Base64AnyFileField, Base64VideoFileField, Base64ImageFileField
from api.models import Tag, Profile, Info, Notification, Company, Category, Blog, Video, FileUploadModel, Support, \
    SupportItem, Banner, Faq, Service
from backend import settings
from drf_writable_nested.serializers import WritableNestedModelSerializer


class ListUserNormalize(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = '__all__'
        abstract = True

    def get_user(self, instance):
        return instance.user.profile.get_admin_user_info()


class FileUploadsSerializer(serializers.ModelSerializer):
    full_path = serializers.SerializerMethodField()

    class Meta:
        model = FileUploadModel
        fields = ('id', 'title', 'file', 'full_path')

    def get_full_path(self, instance):
        if instance.file:
            return instance.get_absolute_url()
        return None


class AdminFileUploadsSerializer(FileUploadsSerializer):
    class Meta:
        model = FileUploadModel
        fields = '__all__'


# Groups
class GroupsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['name']


# Profile update
class ProfileUpdateSerializer(WritableNestedModelSerializer):
    image = FileUploadsSerializer(many=False, allow_null=True)

    class Meta:
        model = Profile
        exclude = ('id', 'user', 'tariff', 'tariff_close_dt', 'type_account',)


# Profile info
class ProfileSerializer(serializers.ModelSerializer):
    image = FileUploadsSerializer(many=False, read_only=True)

    class Meta:
        model = Profile
        exclude = ('user',)


# User info
class UserROSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(read_only=True)
    groups = GroupsSerializer(read_only=True, many=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'is_staff', 'profile', 'groups', 'is_superuser', 'is_active']


# User select (for admin)
class UserSelectSerializer(serializers.ModelSerializer):
    title = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['id', 'title']

    def get_title(self, instance):
        return instance.profile.get_admin_user_info()


# Info
class InfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Info
        exclude = ('id',)


# Notifications
class NotificationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        exclude = ('user',)


# Register
class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=150)
    password = serializers.CharField(max_length=128)
    email = serializers.EmailField()
    first_name = serializers.CharField(max_length=30)
    second_name = serializers.CharField(max_length=30)
    phone = serializers.CharField(max_length=20)
    type_account = serializers.IntegerField(default=0)
    is_subscriber = serializers.BooleanField(default=True)


# Company
class CompanySerializer(WritableNestedModelSerializer):
    logo = FileUploadsSerializer(many=False)

    class Meta:
        model = Company
        exclude = ('created', 'updated', 'user',)
        read_only_fields = ('count_clinics',)


# Admin Company List
class AdminCompanySerializer(CompanySerializer, ListUserNormalize):
    created = serializers.DateTimeField(required=True)

    class Meta:
        model = Company
        fields = '__all__'
        exclude = ()
        read_only_fields = ()


# Admin Company Create or Update
class AdminCreateOrUpdateCompanySerializer(CompanySerializer):
    created = serializers.DateTimeField(required=True)

    class Meta:
        model = Company
        fields = '__all__'
        exclude = ()
        read_only_fields = ()


# Categories
class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        depth = 2


# Tags
class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


# Blog List
class BlogSerializer(WritableNestedModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    categories = CategorySerializer(many=True, read_only=True)
    image = FileUploadsSerializer(many=False, read_only=True)

    class Meta:
        model = Blog
        exclude = ('user',)
        read_only_fields = ('is_accept',)


# Admin Blog List
class AdminBlogSerializer(BlogSerializer, ListUserNormalize):
    class Meta:
        model = Blog
        fields = '__all__'


# Blog Create or update
class BlogCreateOrUpdateSerializer(BlogSerializer):
    categories = serializers.PrimaryKeyRelatedField(
        many=True,
        queryset=Category.objects.all(),
        required=True
    )
    content = serializers.CharField(required=True, allow_null=False)
    tags = TagSerializer(many=True, required=False)
    image = FileUploadsSerializer(many=False)


# Admin Blog Create or update
class AdminBlogCreateOrUpdateSerializer(BlogCreateOrUpdateSerializer):
    created = serializers.DateTimeField(required=True)
    is_accept = serializers.BooleanField(default=False)
    
    class Meta:
        exclude = ()
        read_only_fields = ()
        model = Blog


# Video List
class VideoSerializer(WritableNestedModelSerializer):
    categories = CategorySerializer(many=True, read_only=True)
    file = FileUploadsSerializer(many=False, read_only=True)

    class Meta:
        model = Video
        exclude = ('user',)
        read_only_fields = ('categories', 'file')


# Video Create or update
class VideoCreateOrUpdateSerializer(VideoSerializer):
    categories = serializers.PrimaryKeyRelatedField(
        many=True,
        queryset=Category.objects.all(),
        required=True
    )
    file = FileUploadsSerializer(many=False)
    
    class Meta:
        read_only_fields = ()
        exclude = ('user',)
        model = Video


# Admin Video List
class AdminVideoSerializer(ListUserNormalize, VideoSerializer):
    class Meta:
        fields = '__all__'
        exclude = ()
        model = Video


# Admin Video Create or update
class AdminVideoCreateOrUpdateSerializer(VideoCreateOrUpdateSerializer):
    created = serializers.DateTimeField(required=True)
    
    class Meta:
        exclude = ()
        read_only_fields = ()
        model = Video


# SupportItem
class SupportItemListSerializer(serializers.ModelSerializer):
    files = FileUploadsSerializer(many=True, read_only=True)
    user = UserROSerializer(many=False, read_only=True)

    class Meta:
        model = SupportItem
        exclude = ('support',)


# SupportItem create
class SupportItemCreateOrUpdateSerializer(WritableNestedModelSerializer):
    files = FileUploadsSerializer(many=True, allow_null=True, required=False)

    class Meta:
        model = SupportItem
        exclude = ('user', 'created', 'updated')


# Support
class SupportListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Support
        exclude = ('user',)


# Admin Support List
class AdminSupportListSerializer(ListUserNormalize, SupportListSerializer):
    class Meta:
        model = Support
        exclude = ()


# Banners List
class BannerSerializer(WritableNestedModelSerializer):
    categories = CategorySerializer(many=True, read_only=True)
    file = FileUploadsSerializer(many=False, read_only=True)

    class Meta:
        model = Banner
        exclude = ('user',)
        read_only_fields = ('categories', 'file')


# Banner Create or update
class BannerCreateOrUpdateSerializer(BannerSerializer):
    categories = serializers.PrimaryKeyRelatedField(
        many=True,
        queryset=Category.objects.all(),
        required=True
    )
    file = FileUploadsSerializer(many=False)
    read_only_fields = ()


# Admin Banner List
class AdminBannerSerializer(ListUserNormalize, BannerSerializer):
    class Meta:
        fields = '__all__'
        exclude = ()
        model = Banner


# Admin Banner Create or update
class AdminBannerCreateOrUpdateSerializer(BannerCreateOrUpdateSerializer):
    created = serializers.DateTimeField(required=True)
    
    class Meta:
        fields = '__all__'
        exclude = ()
        model = Banner


# Admin FAQ
class AdminFaqSerializer(serializers.ModelSerializer):
    class Meta:
        model = Faq
        fields = '__all__'


class AdminServiceSerializer(serializers.ModelSerializer):
    categories = CategorySerializer(many=True, read_only=True)

    class Meta:
        model = Service
        fields = '__all__'


class AdminServiceCreateOrUpdateSerializer(AdminServiceSerializer):
    categories = serializers.PrimaryKeyRelatedField(
        many=True,
        queryset=Category.objects.all(),
        required=True
    )
