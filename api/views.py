from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from api.models import Info, Profile, Notification, Company, Blog, Category, Video, FileUploadModel, Support, \
    SupportItem, Banner, Faq, Service
from api.pagination import StandardResultsSetPagination
from api.serializers import UserROSerializer, InfoSerializer, ProfileUpdateSerializer, \
    NotificationsSerializer, RegisterSerializer, CompanySerializer, BlogSerializer, CategorySerializer, \
    BlogCreateOrUpdateSerializer, AdminCompanySerializer, UserSelectSerializer, \
    AdminBlogSerializer, AdminBlogCreateOrUpdateSerializer, VideoSerializer, VideoCreateOrUpdateSerializer, \
    AdminVideoSerializer, AdminVideoCreateOrUpdateSerializer, AdminCreateOrUpdateCompanySerializer, \
    SupportListSerializer, SupportItemListSerializer, SupportItemCreateOrUpdateSerializer, BannerSerializer, \
    BannerCreateOrUpdateSerializer, AdminBannerCreateOrUpdateSerializer, AdminBannerSerializer, \
    AdminSupportListSerializer, AdminFaqSerializer, AdminServiceCreateOrUpdateSerializer, AdminServiceSerializer
from backend.settings import TYPE_ACCOUNT_USER, TYPE_ACCOUNT_PARTNER


class MixedPermissionModelViewSet(ModelViewSet):
    """
    Mixed permission base model allowing for action level
    permission control. Subclasses may define their permissions
    by creating a 'permission_classes_by_action' variable.

    Example:
    permission_classes_by_action = {'list': [AllowAny],
                                    'create': [IsAdminUser]}
    """

    permission_classes_by_action = {}

    def get_permissions(self):
        try:
            # return permission_classes depending on `action`
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            # action is not set return default permission_classes
            return [permission() for permission in self.permission_classes]


# Base Model
class MixedSerializersModelViewSet(ModelViewSet):
    pagination_class = StandardResultsSetPagination
    permission_classes = [permissions.IsAuthenticated]
    serializer_action_classes = {}

    def get_serializer_class(self):
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super().get_serializer_class()


class BaseModelForCurrentUserViewSet(MixedSerializersModelViewSet):
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


# Base Admin Model
class BaseAdminModelViewSet(MixedSerializersModelViewSet):
    permission_classes = [permissions.IsAdminUser]


# Current User
class CurrentUserView(APIView):
    def get(self, request):
        serializer = UserROSerializer(request.user, read_only=True)
        return Response(serializer.data)


# Admin Users
class AdminUserView(ModelViewSet):
    serializer_class = UserROSerializer
    pagination_class = StandardResultsSetPagination
    permission_classes = [permissions.IsAdminUser]

    def get_queryset(self):
        return User.objects.filter(profile__type_account=TYPE_ACCOUNT_USER)


# Admin SelectUsers
class AdminSelectUsersView(APIView):
    permission_classes = [permissions.IsAdminUser]

    def get(self, request):
        users = User.objects.filter(profile__type_account=TYPE_ACCOUNT_PARTNER)
        serializer = UserSelectSerializer(users, read_only=True, many=True)
        return Response(serializer.data)


# Admin Partners
class AdminPartnerView(AdminUserView):
    def get_queryset(self):
        return User.objects.filter(profile__type_account=TYPE_ACCOUNT_PARTNER)


# Получить открытые данные для сайта (информацию)
class InfoView(APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request):
        info = Info.objects.all()
        serializer = InfoSerializer(info, read_only=True, many=True)
        return Response(serializer.data)


# Обновление данных профиля пользователя
class UpdateAccountView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def put(self, request):
        profile = self.request.user.profile
        serializer = ProfileUpdateSerializer(profile, data=self.request.data, partial=True)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Мои уведомления
class MyNotificationView(generics.ListAPIView):
    serializer_class = NotificationsSerializer
    pagination_class = StandardResultsSetPagination
    permission_classes = [permissions.IsAuthenticated]

    def list(self, request, *args, **kwargs):
        Notification.objects.filter(user=request.user, is_read=False).update(is_read=True)
        return super().list(request, *args, **kwargs)

    def get_queryset(self):
        return Notification.objects.filter(user=self.request.user)


# Количество моих уведомлений
class CountMyNotificationView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        count = Notification.objects.filter(user=request.user, is_read=False).count()
        return Response(count)


# Регистрация нового пользователя
class RegisterAccountView(APIView):
    permission_classes = [permissions.AllowAny]

    def post(self, request):
        model = RegisterSerializer(data=request.data)
        if model.is_valid():
            errors = {}
            if len(User.objects.filter(username=model.validated_data.get("username"))) > 0:
                errors['username'] = 'Данный логин уже используется.'
            if len(User.objects.filter(email=model.validated_data.get("email"))) > 0:
                errors['email'] = 'Данный email уже используется.'
            if len(model.validated_data.get("password")) < 8:
                errors['password'] = 'Пароль должен быть не меньше 8 символов. Содержать буквы, цифры и символы.'
            if len(model.validated_data.get("phone")) < 17:
                errors['phone'] = 'Заполните номер полность'
            if len(errors) > 0:
                return Response(errors, status.HTTP_200_OK)
            registration_form = UserCreationForm({
                'username': model.validated_data.get("username"),
                'password1': model.validated_data.get("password"),
                'password2': model.validated_data.get("password"),
            })
            if registration_form.is_valid():
                user = registration_form.save(commit=False)
                user.email = model.validated_data.get("email")
                user.save()

                profile = Profile.objects.get(pk=user.id)
                if profile:
                    profile.phone = model.validated_data.get("phone")
                    profile.first_name = model.validated_data.get("first_name")
                    profile.second_name = model.validated_data.get("second_name")
                    t = model.validated_data.get("is_subscriber")
                    profile.is_subscriber = bool(model.validated_data.get("is_subscriber"))
                    if int(model.validated_data.get("type_account")) == 0:
                        profile.type_account = 0
                    else:
                        profile.type_account = 1
                    profile.save()
                    return Response("User created", status.HTTP_201_CREATED)
            return Response(registration_form.errors, status.HTTP_200_OK)
        return Response(model.errors, status.HTTP_200_OK)


# Компании
class CompanyView(BaseModelForCurrentUserViewSet):
    serializer_class = CompanySerializer
    pagination_class = StandardResultsSetPagination
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return Company.objects.filter(user=self.request.user)


# Admin company
class AdminCompanyView(BaseAdminModelViewSet):
    serializer_class = AdminCreateOrUpdateCompanySerializer
    pagination_class = StandardResultsSetPagination
    queryset = Company.objects.all()
    serializer_action_classes = {
        'list': AdminCompanySerializer
    }


# Blog
class BlogView(BaseModelForCurrentUserViewSet):
    serializer_class = BlogCreateOrUpdateSerializer
    pagination_class = StandardResultsSetPagination
    permission_classes = [permissions.IsAuthenticated]
    serializer_action_classes = {
        'list': BlogSerializer
    }

    def get_queryset(self):
        return Blog.objects.filter(user=self.request.user)


# Admin Blog
class AdminBlogView(BaseAdminModelViewSet):
    serializer_class = AdminBlogCreateOrUpdateSerializer
    pagination_class = StandardResultsSetPagination
    queryset = Blog.objects.all()
    serializer_action_classes = {
        'list': AdminBlogSerializer
    }


# Основные категории
class MainCategoriesView(APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request):
        categories = Category.objects.filter(parent=None)
        serializer = CategorySerializer(categories, read_only=True, many=True)
        return Response(serializer.data)


# Video
class VideoView(BaseModelForCurrentUserViewSet):
    queryset = Video.objects.all()
    serializer_class = VideoCreateOrUpdateSerializer
    serializer_action_classes = {
        'list': VideoSerializer,
    }

    def get_queryset(self):
        return Video.objects.filter(user=self.request.user)


# Admin Video
class AdminVideoView(BaseAdminModelViewSet):
    serializer_class = AdminVideoCreateOrUpdateSerializer
    serializer_action_classes = {
        'list': AdminVideoSerializer,
    }
    queryset = Video.objects.all()


# Загрузка файлов
class FileUploadsView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request):
        response = {}
        for key in request.FILES:
            response[key] = []
            for file in request.FILES.getlist(key):
                file_new = FileUploadModel.objects.create(file=file, user=request.user)
                response[key].append({
                    'id': file_new.id,
                    'title': file_new.title,
                    'full_path': file_new.get_absolute_url(),
                })
        return Response(response, status.HTTP_200_OK)


# Support
class SupportView(BaseModelForCurrentUserViewSet, MixedPermissionModelViewSet):
    queryset = Support.objects.all()
    serializer_class = SupportListSerializer
    permission_classes_by_action = {
        'update': [permissions.IsAdminUser]
    }

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.status = 1
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_create(self, serializer):
        support_model = serializer.save(user=self.request.user)
        SupportItem.objects.create(
            support=support_model,
            message=support_model.description,
            user=self.request.user)

    def get_queryset(self):
        return Support.objects.filter(user=self.request.user)


# Admin Support
class AdminSupportView(SupportView, BaseAdminModelViewSet):
    permission_classes = [permissions.IsAdminUser]
    serializer_class = AdminSupportListSerializer

    def get_queryset(self):
        return Support.objects.all()


# Support get items
class SupportItemsView(APIView):

    def get(self, request, pk):
        support = get_object_or_404(Support, pk=pk)
        if request.user.is_superuser is not True and support.user != request.user:
            return Response({'detail': 'Доступ ограничен'})
        items = SupportItem.objects.filter(support=support)
        paginator = StandardResultsSetPagination()

        page = paginator.paginate_queryset(items, request)
        if page is not None:
            serializer = SupportItemListSerializer(page, many=True)
            return paginator.get_paginated_response(serializer.data)

        serializer = SupportItemListSerializer(items, many=True)
        return Response(serializer.data)


# SupportItem ViewSet
class SupportItemsViewSets(BaseModelForCurrentUserViewSet):
    queryset = SupportItem.objects.all()
    serializer_class = SupportItemCreateOrUpdateSerializer
    permission_classes = [permissions.IsAdminUser]
    permission_classes_by_action = {
        'create': [permissions.IsAuthenticated]
    }

    def perform_create(self, serializer):
        try:
            support = serializer.validated_data['support']
            if support is None \
                    or (support.user != self.request.user
                        and self.request.user.is_superuser is not True):
                raise Exception('Operation access error')
        except:
            raise Exception('Support Error')
        super().perform_create(serializer)

    def get_queryset(self):
        return SupportItem.objects.filter(user=self.request.user)


# Banner
class BannerView(BaseModelForCurrentUserViewSet):
    queryset = Banner.objects.all()
    serializer_class = BannerCreateOrUpdateSerializer
    serializer_action_classes = {
        'list': BannerSerializer,
    }

    def get_queryset(self):
        return Banner.objects.filter(user=self.request.user)


# Admin Banner
class AdminBannerView(BaseAdminModelViewSet):
    serializer_class = AdminBannerCreateOrUpdateSerializer
    serializer_action_classes = {
        'list': AdminBannerSerializer,
    }
    queryset = Banner.objects.all()


# Admin Faq
class AdminFaqView(MixedSerializersModelViewSet):
    serializer_class = AdminFaqSerializer
    permission_classes = [permissions.IsAdminUser]
    queryset = Faq.objects.all()


# Get Faq by type
class FaqByTypeView(APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request, type=0):
        faqs = Faq.objects.filter(type=type)
        serializer = AdminFaqSerializer(faqs, read_only=True, many=True)
        return Response(serializer.data)


# Get FAQ by slug
class FaqBySlugView(APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request, slug=None):
        faq = Faq.objects.get(slug=slug)
        serializer = AdminFaqSerializer(faq, read_only=True, many=False)
        return Response(serializer.data)


# Admin Service
class AdminServiceView(BaseAdminModelViewSet):
    serializer_class = AdminServiceCreateOrUpdateSerializer
    serializer_action_classes = {
        'list': AdminServiceSerializer,
    }
    queryset = Service.objects.all()
