"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework import routers

from .views import CurrentUserView, InfoView, UpdateAccountView, MyNotificationView, CountMyNotificationView, \
    RegisterAccountView, CompanyView, BlogView, MainCategoriesView, AdminUserView, AdminPartnerView, AdminCompanyView, \
    AdminSelectUsersView, AdminBlogView, VideoView, AdminVideoView, FileUploadsView, SupportView, SupportItemsView, \
    SupportItemsViewSets, AdminSupportView, BannerView, AdminBannerView, AdminFaqView, FaqByTypeView, FaqBySlugView, \
    AdminServiceView

# For partners
router = routers.DefaultRouter()
router.register(r'company', CompanyView, basename='company')
router.register(r'blog', BlogView, basename='blog')
router.register(r'video', VideoView, basename='video')
router.register(r'banner', BannerView, basename='banner')
router.register(r'support', SupportView, basename='support')
router.register(r'support-item', SupportItemsViewSets, basename='support-item')

# For admin
router.register(r'admin/users', AdminUserView, basename='admin-users')
router.register(r'admin/partners', AdminPartnerView, basename='admin-partners')
router.register(r'admin/companies', AdminCompanyView, basename='admin-companies')
router.register(r'admin/blog', AdminBlogView, basename='admin-blog')
router.register(r'admin/video', AdminVideoView, basename='admin-video')
router.register(r'admin/banner', AdminBannerView, basename='admin-banner')
router.register(r'admin/support', AdminSupportView, basename='admin-support')
router.register(r'admin/faq', AdminFaqView, basename='admin-faq')
router.register(r'admin/service', AdminServiceView, basename='admin-service')

urlpatterns = [
    path('users/current/', CurrentUserView.as_view()),
    path('info/', InfoView.as_view()),
    path('profile/', UpdateAccountView.as_view()),
    path('notification/my/', MyNotificationView.as_view()),
    path('category/main/', MainCategoriesView.as_view()),
    path('notification/my/count/', CountMyNotificationView.as_view()),
    path('auth/register/', RegisterAccountView.as_view()),
    path('admin/select-users/', AdminSelectUsersView.as_view()),
    path('files/uploads/', FileUploadsView.as_view()),
    path('support/<pk>/items/', SupportItemsView.as_view()),
    path('faq/<int:type>/items/', FaqByTypeView.as_view()),
    path('faq/<slug>/view/', FaqBySlugView.as_view()),
]
