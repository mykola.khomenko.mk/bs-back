from rest_framework.permissions import BasePermission


class IsOwner(BasePermission):
    message = "Forbidden 403"

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


class IsOwnerOrReadOnly(BasePermission):
    message = "Forbidden 403"
    safe_methods = ['GET']

    def has_permission(self, request, view):
        if request.method in self.safe_methods:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if request.method in self.safe_methods:
            return True
        return obj.user == request.user or request.user.is_superuser
